/*- NPM command -*/
// Do not forget the SUDO if you are on MAC or LINUX
// npm i --save-dev autoprefixer browser-sync cssnano gulp gulp-concat gulp-jshint gulp-notify gulp-postcss gulp-sass gulp-sourcemaps gulp-uglify jshint
// npm i --save scrollreveal

/*- Website URL -*/
const devsite = "http://localhost/video/background-video";

/*- Loading modules -*/
const gulp = require("gulp"),
  sass = require("gulp-sass")(require('sass')),
  sourcemaps = require("gulp-sourcemaps"),
  autoprefixer = require("autoprefixer"),
  cssnano = require("cssnano"),
  concat = require("gulp-concat"),
  jshint = require("gulp-jshint"),
  postcss = require("gulp-postcss"),
  notify = require("gulp-notify"),
  uglify = require("gulp-uglify"),
  browserSync = require("browser-sync").create();

/*- Paths -*/
const paths = {
  styles: {
    src : "assets/css/*.scss",
    dest : "assets/dist/css/",
    map : "map/",
  },
  php : {
    src : [
      "./*.php",
      "template/*.php",
      "templates/*.php"
    ]
  },
  html : {
    src : "./*.html"
  }
};

/*- Tasks -*/
const style = () => {
  return gulp
    .src(paths.styles.src)
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write(paths.styles.map))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
}

const php = () => {
  browserSync.reload();
  return;
}

const html = () => {
  browserSync.reload();
  return;
}

const reload = () => {
  browserSync.reload();
}

const watch = async () => {
  browserSync.init({ proxy: devsite });
  gulp.watch("assets/css/*.scss", style).on("change", reload);
  // gulp.watch(paths.scripts.src, script).on("change", reload);
  gulp.watch(paths.html.src, html).on("change", reload);
  gulp.watch("app.js", html).on("change", reload);

  paths.php.src.forEach(path => {
    gulp.watch(path, php).on("change", reload);
  });
}

const watcher = gulp.parallel(style, watch, html, php);

gulp.task("watch", watcher);
gulp.task("css", style)