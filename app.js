//-- start instance et dépendance --//

const video_zone = document.getElementById('video_zone'),
    video = document.getElementById('bgvid'),
    btn = document.getElementById('pause'),
    current_time_element = document.getElementById('current_time_element'),
    duration_time_element = document.getElementById('duration_time_element'),
    video_progress_bar = document.getElementById('video_progress_bar'),
    start_zone = document.getElementById('start'),
    play_button = document.getElementById('start').querySelector('p')

let progress_disabled = false
let puces;

/**
 * This connects to a local file and returns the result
 * @param {string} file
 * @returns json
 */
const init_json_file = (file) => {
    const request = new XMLHttpRequest();
    request.open("GET", file, false);
    request.send(null)
    return JSON.parse(request.responseText);
    // const json_puce = JSON.parse(request.responseText);
}

const json_puces = init_json_file("./puces.json")

//-- end instance et dépendance --//


//-- dynamic puce Zone --//


const insert_puces = (time) => {
    json_puces.forEach(puce => {
        let element = document.getElementById(puce.id)

        if (puce.time.start < time && time < puce.time.end) {
            if (typeof(element) == 'undefined' | element == null) {
                let content = `
                    <div id="${puce.id}" class="puce" style="top : ${puce.coordinate.top}%; left : ${puce.coordinate.left}%;">
                        <div class="dot">
                            <div class="content">
                                <img src="${puce.image}" alt="">
                                <p>${puce.content}</p>
                            </div>
                        </div>
                        <div class="circle"></div>
                        <div class="pulse"></div>
                    </div>
                `
                video_zone.insertAdjacentHTML('afterbegin', content)
                puces = document.querySelectorAll('.puce')
                start_puce_animations()
            }
        } else {
            if (typeof(element) != 'undefined' && element != null) {
                element.remove()
            }
        }

    });
}

// let puces = document.querySelectorAll('.puce')


//-- dynamic puce Zone --//


/**
 * 
 * @param {object} event contain the coordinates of the mouse on the element
 */
let video_coordiante = (event) => {
    let coordinate = {
        x : event.clientX,
        y : event.clientY,
        get coor () {
            return "Coordinates: (" + this.x + "," + this.y + ")"
        }
    }

    let percent = Math.round((coordinate.y / video_zone.offsetHeight) * 100);
    if (percent > 94) {
        display_progress_bar(true)
    } else if (percent < 90) {
        display_progress_bar(false)
    }
}

/**
 * Allows to change the current time of the video when clicking on the slider
 */
let change_time_progress = () => {
    const new_progress = video_progress_bar.value;
    const duration = Math.round(video.duration);
    const new_time = Math.round((new_progress/100) * duration)

    video.currentTime = new_time
}

/**
 * Shows and hides the video progress bar
 * @param {boolean} display If is True, we display. If is False, we hide
 */
let display_progress_bar = (display) => {
    if (display) {
        anime({
            targets : "#reader",
            opacity : 1,
            height : "100px",
            easing : 'easeInOutSine',
            duration : 200
        })
    } else {
        anime({
            targets : "#reader",
            opacity : 0,
            height : "50px",
            easing : 'easeInOutSine',
            duration : 200
        })
    }
}

let puce_anime_mouse_enter = (puce) => {
    const dot = puce.querySelector('.dot');
    const content = dot.querySelector('.content');
    const content_img = content.querySelector('img');
    // Dot size
    const dot_size = 200;

    // End of current animations
    anime.remove(dot);
    anime.remove(content_img);
    anime.remove(content);

    // Dot enlargement
    anime({
        targets : dot,
        easing : 'easeOutExpo',
        width : dot_size,
        height : dot_size,
        duration : 3000
    })

    // Display the content picture
    anime({
        targets : content_img,
        easing : 'easeOutExpo',
        width : 100,
        duration : 2000
    })

    // Display the content
    anime({
        targets : content,
        easing : 'easeOutExpo',
        opacity : 100,
        duration : 2000
    })
}

let puce_anime_mouse_leave = (puce) => {
    const dot = puce.querySelector('.dot');
    const content = dot.querySelector('.content');
    const content_img = content.querySelector('img');
    const dot_size = 8;
    anime.remove(content_img);
    anime.remove(content);
    anime.remove(dot);
    
    anime({
        targets : content_img,
        easing : 'easeOutExpo',
        width : 0,
        duration : 500
    })

    anime({
        targets : content,
        easing : 'easeOutExpo',
        opacity : 0,
        duration : 500
    })

    anime({
        targets : dot,
        easing : 'easeOutExpo',
        width : dot_size,
        height : dot_size,
        duration : 2500
    })
}

let puce_anime_click = (puce) => {
    const dot = puce.querySelector('.dot');
    const content = dot.querySelector('.content');
    const content_img = content.querySelector('img');
    const dot_size = 10000;
    anime.remove(content_img);
    anime.remove(content);
    anime.remove(dot);
    
    anime({
        targets : content_img,
        easing : 'easeOutExpo',
        width : 0,
        duration : 500
    })

    anime({
        targets : content,
        easing : 'easeOutExpo',
        opacity : 0,
        duration : 500
    })

    anime({
        targets : dot,
        easing : 'easeOutExpo',
        width : dot_size,
        height : dot_size,
        duration : 2500
    })
}

let start_puce_animations = () => {
    
    const puce_anime = anime({
        targets: ".pulse",
        scale: 3,
        loop: true,
        easing: 'easeInOutSine',
        opacity: 0,
        duration: 900,
        direction: 'alternate'
    });

    // Dot animation //
    puces.forEach(puce => {
        const dot = puce.querySelector('.dot');
        const content = dot.querySelector('.content');
        const content_img = content.querySelector('img');
        
        /**
         * Displays the tooltip
         */
        puce.addEventListener('mouseenter', (event) => {
            // Dot size
            const dot_size = 200;

            // End of current animations
            anime.remove(dot);
            anime.remove(content_img);
            anime.remove(content);

            // Dot enlargement
            anime({
                targets : dot,
                easing : 'easeOutExpo',
                width : dot_size,
                height : dot_size,
                duration : 3000
            })

            // Display the content picture
            anime({
                targets : content_img,
                easing : 'easeOutExpo',
                width : 100,
                duration : 2000
            })

            // Display the content
            anime({
                targets : content,
                easing : 'easeOutExpo',
                opacity : 100,
                duration : 2000
            })
        })

        /**
         * Hide the tooltip
         */
        puce.addEventListener('mouseleave', (event) => {
            const dot_size = 8;
            anime.remove(content_img);
            anime.remove(content);
            anime.remove(dot);
            
            anime({
                targets : content_img,
                easing : 'easeOutExpo',
                width : 0,
                duration : 500
            })

            anime({
                targets : content,
                easing : 'easeOutExpo',
                opacity : 0,
                duration : 500
            })

            anime({
                targets : dot,
                easing : 'easeOutExpo',
                width : dot_size,
                height : dot_size,
                duration : 2500
            })

        })

        /**
         * Change page
         */
        puce.addEventListener('click', (event) => {
            const dot_size = 10000;
            anime.remove(content_img);
            anime.remove(content);
            anime.remove(dot);
            
            anime({
                targets : content_img,
                easing : 'easeOutExpo',
                width : 0,
                duration : 500
            })

            anime({
                targets : content,
                easing : 'easeOutExpo',
                opacity : 0,
                duration : 500
            })

            anime({
                targets : dot,
                easing : 'easeOutExpo',
                width : dot_size,
                height : dot_size,
                duration : 2500
            })
        })
    });
}

/**
 * Checks if the video is on or off and acts accordingly
 */
const the_state = () => {
    const pause_to_play = document.getElementById('from_pause_to_play');
    const play_to_pause = document.getElementById('from_play_to_pause');

    let state = "paused";

    if (video.paused) {
        video.play();
        play_to_pause.beginElement();
    } else {
        video.pause();
        pause_to_play.beginElement();
    }
}

/**
 * Displays the current and final time on the HTML page
 */
const video_times = () => {
    const time = {
        current : {
            minutes : Math.floor(video.currentTime / 60),
            get seconds() {
                return Math.floor(video.currentTime) - this.minutes * 60
            }
        },
        duration : {
            minutes : Math.floor(video.duration / 60),
            get seconds () {
                return Math.floor(video.duration - this.minutes * 60)
            }
        }
    }

    insert_puces(Math.round(video.currentTime))
    // start_puce_animations()

    let value = (Math.round(video.currentTime) / Math.round(video.duration)) * 100

    if (!progress_disabled) {
        video_progress_bar.value = Math.round(value);
    }

    current_time_element.innerHTML = `${time.current.minutes}:${time.current.seconds < 10 ? '0'+time.current.seconds : time.current.seconds}`
    duration_time_element.innerHTML = `${time.duration.minutes}:${time.duration.seconds < 10 ? '0'+time.duration.seconds : time.duration.seconds}`
}

/**
 * Performs the first start of the video
 */
let play_video = () => {
    const duration = 1000
    anime({
        targets : '#start',
        opacity : 0,
        display : 'none',
        duration : duration,
        easing: 'easeInOutSine',
        begin : () => {
            // We wait until the animation ends to start the video
            setTimeout(() => {
                document.querySelector('#start').style.display = "none"
                the_state()
            }, duration);
        }
    })
}

video.addEventListener('timeupdate', () => { video_times() })
btn.addEventListener('click', () => { the_state() })
video_zone.addEventListener('mousemove', (event) => { video_coordiante(event) })
video_progress_bar.addEventListener('mousedown' ,() => { progress_disabled = true })
video_progress_bar.addEventListener('mouseup' ,() => { 
    change_time_progress()
    progress_disabled = false 
})
play_button.addEventListener('click', (event) => { play_video() })